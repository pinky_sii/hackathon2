# Hackathon II: Web Application #
(Rapid App Dev Tri 3/2017 - 2018)

Due:  May 17th, 2018 @ 11:59pm

Our theme for this hackathon is "Better College". Each team will come up with an application that will make (your) life in college better! For examples, the applications can be anything from the following list but not limited to:

* In-classroom applications 
* Class material management
* Course planning 
* Homework submission management
* etc.

## basic-spa-vue-firebase

A Vue.js project

## Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn start

# build for production with minification
yarn build

# build for production and view the bundle analyzer report
yarn build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Visit demo project here 
https://hackaton2-7d785.firebaseapp.com/

## Resources ##
https://firebase.google.com/docs/auth/web/manage-users 

https://vuetifyjs.com/en/

